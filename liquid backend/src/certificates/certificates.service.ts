import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import * as dateformat from 'dateformat';


@Injectable()
export class ConverterService {
    constructor(private connection: Connection){}
    async getcertificate(username, course_id){
       const queryRunner = this.connection.createQueryRunner();
       const queryForId = `SELECT id FROM auth_user WHERE username = '${username}'`;
       const userId = await queryRunner.query(queryForId);
       const query = `SELECT auth_userprofile.name, grades_persistentcoursegrade.course_id,grades_persistentcoursegrade.letter_grade, course_overviews_courseoverview.display_name , grades_persistentcoursegrade.passed_timestamp, grades_persistentcoursegrade.created,grades_persistentcoursegrade.modified,grades_persistentcoursegrade.course_edited_timestamp,grades_persistentcoursegrade.percent_grade
       FROM auth_userprofile 
       INNER JOIN grades_persistentcoursegrade 
       ON auth_userprofile.id=grades_persistentcoursegrade.user_id 
       INNER JOIN course_overviews_courseoverview 
       ON course_overviews_courseoverview.id=grades_persistentcoursegrade.course_id
       WHERE auth_userprofile.user_id = ${userId[0].id}
       AND letter_grade = 'Pass'
       AND grades_persistentcoursegrade.course_id = '${course_id}'` 
       const certificate = await queryRunner.query(query);
       console.log(certificate);
       if(!certificate){
        return 'No certificates.'
        }
        else{
        const Certificate = certificate[0];
        Certificate.passed_timestamp = dateformat(Certificate.passed_timestamp,"yyyy-mm-dd");
        Certificate.created = dateformat(Certificate.created,"yyyy-mm-dd");
        Certificate.modified = dateformat(Certificate.modified,"yyyy-mm-dd");
        Certificate.course_edited_timestamp = dateformat(Certificate.course_edited_timestamp,"yyyy-mm-dd");
        console.log(Certificate);
        return Certificate;
        }
    }

    async getallcertificatesforstudent(username){
        const queryRunner = this.connection.createQueryRunner();
        const queryForId = `SELECT id FROM auth_user WHERE username = '${username}'` 
        const userId = await queryRunner.query(queryForId);
        const queryForCertificates = `SELECT auth_userprofile.name, grades_persistentcoursegrade.course_id,grades_persistentcoursegrade.letter_grade, course_overviews_courseoverview.display_name , grades_persistentcoursegrade.passed_timestamp, grades_persistentcoursegrade.created,grades_persistentcoursegrade.modified,grades_persistentcoursegrade.course_edited_timestamp,grades_persistentcoursegrade.percent_grade
        FROM auth_userprofile 
        INNER JOIN grades_persistentcoursegrade 
        ON auth_userprofile.id=grades_persistentcoursegrade.user_id 
        INNER JOIN course_overviews_courseoverview 
        ON course_overviews_courseoverview.id=grades_persistentcoursegrade.course_id
        WHERE auth_userprofile.user_id = ${userId[0].id}
        AND letter_grade = 'Pass'`
        const certificates: any[] = await queryRunner.query(queryForCertificates);
        if(certificates.length > 0){
        return certificates;
    }
        else
        return 'No certificates.'
     }

    // async getallcertificatesforstudent(username){
    //     const query = `SELECT auth_userprofile.name, grades_persistentcoursegrade.course_id,
    //     grades_persistentcoursegrade.letter_grade,course_overviews_courseoverview.display_name
    //     FROM auth_userprofile
    //     INNER JOIN grades_persistentcoursegrade ON
    //     auth_userprofile.id = grades_persistentcoursegrade.user_id
    //     INNER JOIN course_overviews_courseoverview ON
    //     course_overviews_courseoverview.id = grades_persistentcoursegrade.course_id;`
    //     const queryRunner = this.connection.createQueryRunner();
    //     const certificates: any[] = await queryRunner.query(query);
    //     if(certificates.length > 0){
    //      return certificates;
    //  }
    //      else
    //      return 'No certificates.'
    //  }

}
