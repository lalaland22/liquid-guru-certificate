import { Controller, Get, Param } from '@nestjs/common';
import { ConverterService } from './certificates.service';

@Controller('certificates')
export class ConverterController {
    constructor(private converterservice: ConverterService){}
    @Get('getcertificate/:username/:course_id')
    getCertificateById(@Param('username')id, @Param('course_id')course_id){
        return this.converterservice.getcertificate(id, course_id);
    }

    @Get('getcertificates/:username')
    getallCertificatesForStudent(@Param('username')id){
        return this.converterservice.getallcertificatesforstudent(id);
    }
}


