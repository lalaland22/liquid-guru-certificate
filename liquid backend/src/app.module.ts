import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConverterController } from './certificates/certificates.controller';
import { ConverterService } from './certificates/certificates.service';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'pbi-oxadevdg.westeurope.cloudapp.azure.com',
      port: 3306,
      username: 'powerbioxa',
      password: 'GenRaT8Pass!',
      database: 'edxapp',
      entities: [],
      synchronize: true,
    }),
  ],
  controllers: [AppController, ConverterController],
  providers: [AppService, ConverterService],
})

export class AppModule {}